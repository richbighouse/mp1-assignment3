package com.richbighouse.mp1_assignment3;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class DescriptionActivity extends Activity {

    EditText textName;
    EditText textId;
    EditText textSex;
    EditText textDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        Student student = getStudent();

        textName = findViewById(R.id.description_text_name);
        textId = findViewById(R.id.description_text_id);
        textSex = findViewById(R.id.description_text_sex);
        textDescription= findViewById(R.id.description_text_description);

        textName.setText(student.getName());
        textId.setText(student.getId());
        textSex.setText(student.getSex());
        textDescription.setText(student.getDescription());
    }

    private Student getStudent() {
        Intent intent = getIntent();
        int studentPosition = intent.getIntExtra("studentPosition", 0);
        return StudentList.getInstance().getStudentList().get(studentPosition);
    }

    public void remove(View view) {
        StudentList.getInstance().getStudentList().remove(getStudent());
        finish();
    }

    public void save(View view) {
        Student student = getStudent();

        student.setName(textName.getText().toString());
        student.setId(textId.getText().toString());
        student.setSex(textSex.getText().toString());
        student.setDescription(textDescription.getText().toString());
    }
}
