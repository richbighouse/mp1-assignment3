package com.richbighouse.mp1_assignment3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class InputActivity extends Activity {

    EditText textName;
    EditText textId;
    EditText textSex;
    EditText textDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        textName = findViewById(R.id.input_text_name);
        textId = findViewById(R.id.input_text_id);
        textSex = findViewById(R.id.input_text_sex);
        textDescription= findViewById(R.id.input_text_description);
    }

    public void add(View view) {
        Student student = new Student();
        student.setName(textName.getText().toString());
        student.setId(textId.getText().toString());
        student.setSex(textSex.getText().toString());
        student.setDescription(textDescription.getText().toString());

        StudentList.getInstance().getStudentList().add(student);

        Toast.makeText(this, "Added " + textName.getText().toString(), Toast.LENGTH_LONG).show();

        textName.setText("");
        textId.setText("");
        textSex.setText("");
        textDescription.setText("");
    }
}
