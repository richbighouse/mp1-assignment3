package com.richbighouse.mp1_assignment3;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class StudentAdapter extends ArrayAdapter {

    public StudentAdapter(@NonNull Context context, List<Student> students) {
        super(context, R.layout.student_row, students);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater studentInflator = LayoutInflater.from(getContext());
        View studentRow = studentInflator.inflate(R.layout.student_row, parent, false);

        Student student = (Student) getItem(position);

        TextView nameTextView = studentRow.findViewById(R.id.row_name);
        TextView idTextView = studentRow.findViewById(R.id.row_id);
        ImageView imageView = studentRow.findViewById(R.id.row_image);

        if (student.getImageId() == 0) {
            choosePicture(student);
        }

        nameTextView.setText(student.getName());
        idTextView.setText(student.getId());
        imageView.setImageResource(student.getImageId());

        return studentRow;
    }

    private void choosePicture(Student student) {
        int picture = 1 + (int) (Math.random() * 3); //random number between 1 and 3
        if (student.getSex().equalsIgnoreCase("male") || student.getSex().equalsIgnoreCase("m")) {
            if (picture == 1) {
                student.setImageId(R.drawable.boy1);
            } else if (picture == 2) {
                student.setImageId(R.drawable.boy2);
            } else {
                student.setImageId(R.drawable.boy3);
            }
        } else {
            if (picture == 1) {
                student.setImageId(R.drawable.girl1);
            } else if (picture == 2) {
                student.setImageId(R.drawable.girl2);
            } else {
                student.setImageId(R.drawable.girl3);
            }
        }
    }
}
