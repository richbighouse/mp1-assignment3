package com.richbighouse.mp1_assignment3;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

    private final String VALID_USERNAME = "admin";
    private final String VALID_PASSWORD = "admin";

    EditText textUsername;
    EditText textPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        textUsername = findViewById(R.id.login_text_username);
        textPassword = findViewById(R.id.login_text_password);
    }

    public void login(View view) {
        String username = textUsername.getText().toString();
        String password = textPassword.getText().toString();

        if (username.equals(VALID_USERNAME) && password.equals(VALID_PASSWORD)) {
            Intent intent = new Intent(this, MenuActivity.class);
            textPassword.setText("");
            startActivity(intent);
        } else {
            textPassword.setText("");
            Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_LONG).show();
        }
    }
}
