package com.richbighouse.mp1_assignment3;

import java.util.ArrayList;
import java.util.List;

public class StudentList {
    private static StudentList instance = null;

    private List<Student> studentList;

    private StudentList() {
        studentList = new ArrayList<>();
    }

    public static StudentList getInstance() {
        if (instance == null) {
            instance = new StudentList();
        }
        return instance;
    }

    public List<Student> getStudentList() {
        return this.studentList;
    }
}
